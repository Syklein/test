<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
	//public $asuntos=['Reclamo','Solicitud','Queja'];
    public $asuntosJSON='[
             {"id": 1,"nombre": "Reclamo"},
             {"id": 2,"nombre": "Solicitud"},
             {"id": 3,"nombre": "Queja"}
         ]';
    public $diccionarioJSON='[
             {"nombre": "viagra","puntaje": 5 },
             {"nombre": "oferta/s","puntaje": 4 },
             {"nombre": "buy","puntaje": 5},
             {"nombre": "contactanos","puntaje": 3},
             {"nombre": "tarifas","puntaje": 2 },
             {"nombre": "stock","puntaje": 1 }
         ]';
    public function run()
    {
        $asuntos=json_decode($this->asuntosJSON);
    	foreach ($asuntos as $asunto) {
    		DB::table('subjects')->insert([
            'id' => $asunto->id,
            'nombre' => $asunto->nombre
            
        ]);
    	}

        $palabras=json_decode($this->diccionarioJSON);
        foreach ($palabras as $palabra) {
            DB::table('diccionarios')->insert([
            'nombre' => $palabra->nombre,
            'puntaje' => $palabra->puntaje
            
        ]);
        }
        
    }
}