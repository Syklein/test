<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'subjectId','body','fromName', 'fromEmail','toEmail'
    ];

    public function subject()
    {
        return $this->belongsTo('App\Subject','subjectId');
    }
}
