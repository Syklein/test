<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Message;
use App\diccionario;
use Illuminate\Support\Facades\Mail;
use  App\Mail\Correo;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;

class formularioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $cantidadM= Message::count();
         return view('listCorreo')->with("cantidadTotal",$cantidadM);

    }

    public function getTabla(){
        $cantidadM= Message::count();        

        $correos = Message::leftJoin('subjects', 'messages.subjectId', '=', 'subjects.id')
            ->select('messages.id','messages.fromName','messages.fromEmail','messages.body', "messages.created_at",'messages.spamScore','subjects.nombre as asunto', 
                DB::raw('COUNT(messages.subjectId) as cantidad'),
                DB::raw("CONCAT(ROUND((count(messages.subjectId)*100)/".$cantidadM.",2),'%') as 'porcentaje'"))
            ->groupBy('messages.subjectId')->get(); //nueva consulta
        

        /*$correos = DB::select("select m.id,m.fromName,m.fromEmail,m.body,DATE_FORMAT(m.created_at,'%d/%m/%y') as 'created_at',m.spamScore, s.nombre as asunto, count(s.id) as 'cantidad',total.cantTotal as 'totalM', CONCAT(ROUND((count(s.id)*100)/total.cantTotal,2),'%') as 'porcentaje' from messages as m, subjects as s, (select count(*) as 'cantTotal' from messages) as total Where m.subjectId = s.id group by s.id");  //Old*/

        
 
        return Datatables::of($correos)->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $mensaje= new Message();
        $mensaje->subjectId = $request->asunto;
        $mensaje->body = $request->mensaje;
        $mensaje->fromName = $request->nombre;
        $mensaje->fromEmail = $request->correo;
        $mensaje->toEmail = 'soporte@agrosty.com';
        $mensaje->spamScore = $this->spamScore(explode(" ", $request->mensaje));
        $mensaje->save();
        Mail::to($mensaje->toEmail)->send(new Correo($mensaje));
        return $mensaje;
 
    }

    private function spamScore($palabras){
        $diccionario=diccionario::select('nombre','puntaje')->get();//Las palabras del diccionario
        $nDiccionario=count($diccionario);//cantidad de palabras que tiene el diccionario
        $sum=0;//se encargara de ir sumando los puntajes
        $spam=0;
        foreach ($diccionario as $pD) {//pD es igual a palabra diccionario
            # Aqui vamos a comparar todas las palabras del diccionario con $palabras que es un arreglo que contienes todas las palabras del mensaje.
            foreach ($palabras as $pM) {//$pM palabra del mensaje
                if(strcmp(strtoupper($pD->nombre),strtoupper($pM)) === 0){ // si la palabra no debe ser exacta porque puede ser en plural puede tener dos puntos o un punto pegado se usa ofertas.: usamos strpos($pM,$pD->nombre) !== false
                    $sum+=$pD->puntaje;
                }
            }
        }
        if($nDiccionario != 0){

            $total = $sum/$nDiccionario;
            if($total > 2.5)
                $spam=1;

        }
        return $spam;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
