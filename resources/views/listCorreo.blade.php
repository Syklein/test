@extends('layouts.app')

@push('styles')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
  <style type="text/css">
    body{
        font-size: 1.5rem !important;
    }      
  </style>
@endpush

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                  <h1 class="float-left">Listados del Correo</h1>
                  <div class="float-right"><label>Cantidad de Mensajes: {{$cantidadTotal}}</label>
                  </div>
                </div> 
            <div class="card-body">    

        	<table id="task" class="table table-hover table-condensed">
		    <thead>
		        <tr>
                <th class="text-center">ID</th>
		            <th class="text-center">Nombre</th>		         
		            <th class="text-center">Correo</th>
                <th class="text-center">Asunto</th>
		            <th class="text-center">Mensaje</th>
		            <th class="text-center">Fecha</th>
                <th class="text-center">Cantidad</th>
                <th class="text-center">Porcentaje</th>
                    
		        </tr>
		    </thead>
		     


      </table>
  </div>
        </div>
    </div>
    </div>
    </div>

@endsection
    @section('javascript')
  
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" defer ></script>
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>  
 
 
  
<script type="text/javascript">
   window.onload = open;
        function open(){
        $(document).ready(function() {
        oTable = $('#task').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('getCorreos') }}",
            "columns": [
                {data: 'id', name: 'id'},
                {data: 'fromName', name: 'fromName'},
                {data: 'fromEmail', name: 'fromEmail'},
                {data: 'asunto', name: 'asunto'},
                {data: 'body', name: 'body'},
                {data: 'created_at', name: 'created_at'},
                {data: 'cantidad', name: 'cantidad'},
                {data: 'porcentaje', name: 'porcentaje'},
                
            ]
        });

    });

    }
     
</script>  
@endsection  



