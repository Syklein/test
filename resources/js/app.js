
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


require('./bootstrap');
window.Vue = require('vue');
Vue.use(BootstrapVue);



Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('dropdown-component', require('./components/dropdownComponent.vue').default);

const app = new Vue({
    el: '#app',
});
